# stocks-ema

Exponential moving average prediction and stock buying/selling

# Installation
```shell
pip install -r requirements.txt
```

# Setup RH Password
```shell
export RH_PASS=<whatever>
```

