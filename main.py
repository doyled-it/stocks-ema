import os
import pandas as pd
import robin_stocks as rs
import matplotlib.pyplot as plt


from stocks_ema import utils
from stocks_ema import test


def main():
    crypto = True
    scatter = True

    interval_choices = ['15second', '5minute', '10minute', 'hour', 'day', 'week']
    span_choices = ['hour', 'day', 'week', 'month', '3month', 'year', '5year']

    username = 'michaeldoyle1994@gmail.com'
    ticker = 'SPY'
    crypto_ticker = 'ETH'
    interval = 'hour'
    span = 'week'
    ex_avg_small_span = 3
    ex_avg_large_span = 8
    starting_cash = 500.0

    ex_avg_small_str = utils.get_interval_string(interval, ex_avg_small_span)
    ex_avg_large_str = utils.get_interval_string(interval, ex_avg_large_span)

    # Logging in
    login = rs.login(username=username, password=os.environ.get('RH_PASS'))

    if crypto:
        data = rs.crypto.get_crypto_historical(crypto_ticker, interval, span, '24_7')
    else:
        data = rs.stocks.get_historicals(ticker, span=span)

    if data is None:
        Exception('Data returned nothing...')

    # Setup Pandas DataFrame format
    df = {'begins_at': [],
          'open_price': [],
          'close_price': [],
          'high_price': [],
          'low_price': [],
          'volume': [],
          'session': [],
          'interpolated': []}

    # Retain metadata
    meta = {'bounds': data['bounds'],
            'interval': data['interval'],
            'span': data['span'],
            'symbol': data['symbol'],
            'id': data['id']}

    # Put into Pandas DataFrame
    for interval in data['data_points']:
        for key in interval.keys():
            df[key].append(interval[key])

    df = pd.DataFrame(data=df)
    # Convert all numeric values to floats
    df.close_price = df.close_price.astype(float)
    df.open_price = df.open_price.astype(float)
    df.high_price = df.high_price.astype(float)
    df.low_price = df.low_price.astype(float)

    ewa_small = df.close_price.ewm(span=ex_avg_small_span, adjust=False).mean()
    ewa_large = df.close_price.ewm(span=ex_avg_large_span, adjust=False).mean()

    plt.figure()
    df.plot(y='close_price', label=crypto_ticker)
    if scatter:
        plt.scatter(list(range(len(df))),
                    ewa_small, s=1,
                    label=f'Exp Avg {ex_avg_small_str}')
        plt.scatter(list(range(len(df))),
                    ewa_large, s=1,
                    label=f'Exp Avg {ex_avg_large_str}')
    else:
        plt.plot(ewa_small, label=f'Exp Avg {ex_avg_small_str}')
        plt.plot(ewa_large, label=f'Exp Avg {ex_avg_large_str}')
    plt.legend(loc='lower left')
    plt.show()
    plt.savefig('test.png')

    profit = test.test(df, ewa_small, ewa_large, starting=starting_cash)
    print(f'Crypto: {crypto_ticker}')
    print(f'Profit: {profit}')
    print(f'Percent Profit: {round(profit/starting_cash*100, 2)}%')

if __name__ == '__main__':
    main()
    