import pandas as pd

def test(dataframe: pd.core.frame.DataFrame,
         ewa_small: pd.core.series.Series,
         ewa_large: pd.core.series.Series,
         price_name: str = 'close_price',
         starting: float = 500.0):
    """Test different exponential averages and calculate profits

    Arguments:
        dataframe (pandas.DataFrame): historical dataframe to test with
        ewa_small (pandas.Series): historical exponential average with small span
        ewa_large (pandas.Series): historical exponential average with large span
        price_name (str): 'close_price', 'open_price', etc.
        starting (float): starting cash for time frame
    Returns:
        profit (float): 
    """
    if ewa_small[0] > ewa_large[0]:
        bull = True
        bitcoins = starting/dataframe[price_name][0]
    else:
        cash = starting
        bull = False


    for i, price in enumerate(dataframe[price_name]):
        if ewa_small[i] > ewa_large[i] and bull==True:
            # We are in a bull market and we're already bought in
            ## Don't do anything
            continue
        elif ewa_small[i] < ewa_large[i] and bull==True:
            # We hit the inflection point and should sell
            ## We're now in a bear market
            cash = price * bitcoins
            bull = False
        elif ewa_small[i] < ewa_large[i] and bull==False:
            # We are still in a bear market and should not do anything
            continue
        elif ewa_small[i] > ewa_large[i] and bull==False:
            # We hit the upward inflection point and should buy
            ## We're now in a bull market
            bitcoins = cash/price
            bull = True
        else:
            continue

    if bull:
        # Sell at the end of the dataframe (if bull market) to judge profit
        cash = dataframe[price_name][len(dataframe)-1]*bitcoins

    profit = cash - starting

    return profit