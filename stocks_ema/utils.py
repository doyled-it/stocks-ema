from typing import Tuple

def get_interval_string(interval: str,
                        avg_span: int) -> str:
    """Convert robinhood data interval to string

    Arguments:
        interval (str): robinhood interval
        avg_span (int): exponential average short span
    Returns:
        strings to make exponential average spans human-readable
    """

    interval_check = ['15second', '5minute', '10minute', 'hour', 'day', 'week']

    if interval == '15second':
        avg_span_str = f'{15 * avg_span} seconds'
    elif interval == '5minute':
        avg_span_str = f'{5 * avg_span} minutes'
    elif interval == '10minute':
        avg_span_str = f'{10 * avg_span} minutes'
    elif interval == 'hour':
        avg_span_str = f'{avg_span} hours'
    elif interval == 'day':
        avg_span_str = f'{avg_span} days'
    elif interval == 'week':
        avg_span_str = f'{avg_span} weeks'
    else:
        Exception(f'interval must be one of the following:{val for val in interval_check}')
    return avg_span_str